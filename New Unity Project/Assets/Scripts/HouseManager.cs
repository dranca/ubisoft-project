﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using MiniJSON;

public class HouseManager : MonoBehaviour {

    public GameObject townPrefab;
    public bool shouldUseGivenData;

    private ArrayList towns;
	// Use this for initialization
	void Start () {
        
        // initialization
        this.towns = new ArrayList();
        // i tend to use Start for initialization instead of Awake()
       if(shouldUseGivenData)
       {
           this.useGivenData();
           // i call generate towns here bc the second method deals with networking and because concurency
           this.generateTownsFromPlainObjects();
       }
       else
       {
           this.getHousesFromWeb("http://localhost:8080/MapService-1.0.0/services/mapService/getCities");
       }
    }
	
    public void useGivenData()
    {
        
        PlainTown town1 = new PlainTown("Town 1", 34.23f, 12.12f, 3, 4);
        towns.Add(town1);
        PlainTown town2 = new PlainTown("Town 2", 23.34f, 15.55f, 2, 3);
        towns.Add(town2);
        PlainTown town3 = new PlainTown("Town 3", 50f, 15.12f, 1, 2);
        towns.Add(town3);
        PlainTown town4 = new PlainTown("Town 4", 30.34f, 18.55f, 1, 4);
        towns.Add(town4);

    }
    public void generateTownsFromPlainObjects()
    {
        foreach (PlainTown town in towns)
        {
            GameObject townGO = GameObject.Instantiate(townPrefab) as GameObject;
            townGO.transform.SetParent(this.transform);

            townGO.transform.localPosition = new Vector3(town.posX, town.posY);
            town.actualGameoObject = townGO;
        }
    }

    public void getHousesFromWeb(string url)
    {
        // i am kinda lazy and i don't really like java so i will explain step by step how i will do it in a real life situation.: ready? good
        // since this is most likely a rest call i will use www

        WWW www = new WWW(url);
        StartCoroutine(WaitForRequest(www));
       

    }

    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;

        // check for errors
        if (www.error != null)
        {
            string s = www.text;
            // i use MiniJson for this json and no i can give the dict as a parameter.
            var dict = Json.Deserialize(s) as Dictionary<string, string>;
            this.generateTownsForDictionary(dict);
        }
        
    }

    public void generateTownsForDictionary (Dictionary<string,string> dict)
    {
        foreach(KeyValuePair<string,string> town in dict)
        {
            // now i create each object with the received data....
            PlainTown town1 = new PlainTown("Town 1", 34.23f, 12.12f, 3, 4);
            towns.Add(town1);
        }
        // don't forget to add them
        this.generateTownsFromPlainObjects();
    }

	// Update is called once per frame
	void Update () {
        foreach (PlainTown town in towns)
        {
            town.actualGameoObject.SetActive(!(Map.zoomLevel > town.zoomMax || Map.zoomLevel < town.zoomMin));
        }
	}



    // used in the calculation of shortest distance
    public static float getDistanceBetweenTwoTowns(PlainTown pt1, PlainTown pt2)
    {
        return Vector3.Distance(pt1.actualGameoObject.transform.position, pt2.actualGameoObject.transform.position);
    }
}


// this class is just an object that holds information about a particular house. seems easyer
public class PlainTown
{
    
    public string name;
    public float posX;
    public float posY;
    public int zoomMin, zoomMax;
    public GameObject actualGameoObject;
    public PlainTown(string name, float posX, float posY, int zoomMin, int zoomMax)
    {
        this.name = name;
        this.posX = posX;
        this.posY = posY;
        this.zoomMin = zoomMin;
        this.zoomMax = zoomMax;
    }
}