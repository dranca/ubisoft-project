﻿using UnityEngine;
using System.Collections;

public class CameraMover : MonoBehaviour {

    public float mapSpeed;
    public GameObject Magic3DObject;

	// Update is called once per frame
	void Update () {
        
        
        // i know this is a bad idea to have the same code twice 
        float yPos = Input.GetAxis("Vertical") * mapSpeed;
        float xPos = Input.GetAxis("Horizontal") * mapSpeed;

        Vector3 diff = new Vector3(yPos, xPos, 0);

        this.transform.RotateAround(Magic3DObject.transform.position, diff, mapSpeed * Time.deltaTime);
	}
}
