﻿using UnityEngine;
using System.Collections;

public class MoveScript : MonoBehaviour {

    public RectTransform mapTransform;
    public float mapSpeed;

    

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        float yPos = Input.GetAxis("Vertical") * mapSpeed;
        float xPos = Input.GetAxis("Horizontal") * mapSpeed;
        this.transform.Translate( - xPos, - yPos, 0);
	}
}
