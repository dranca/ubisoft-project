﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Map : MonoBehaviour {


    // i just ask for it. don't need it yes... maybe later
    public MoveScript moveScript;
    public Transform movingPartsTransform;
    public Slider zoom;

    private int min = 1;
    private int max = 4;

    public static float zoomLevel;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        
        // this might be overkill but you never know
        zoomLevel = Mathf.Clamp(zoomLevel, min, max);

        movingPartsTransform.localScale = new Vector3(zoomLevel, zoomLevel);


	}

    public void reSetZoom()
    {
        Map.zoomLevel = 1;
    }

    public void adjustZoom()
    {
        Map.zoomLevel = this.zoom.value;
    }
}
